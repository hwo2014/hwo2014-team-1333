package noobbot;

public class Car {
	CarID id;
	String angle;
	PiecePosition piecePosition;
}
class CarID{
	public String name;
	public String color;
}
class PiecePosition{
	String pieceIndex;
	String inPieceDistance;
	Lane lane;
	int lap;
}
class Lane {
	int startLaneIndex;
	int endLaneIndex;
}
