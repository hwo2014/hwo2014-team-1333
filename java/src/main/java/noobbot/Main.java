package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;

import com.google.gson.Gson;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
        CONSTANTS.BOTNAME=botName;
        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        //new Main(reader, writer, new Join(botName, botKey));  "keimola
        new Main(reader, writer, new JoinRace(botName, botKey, "keimola",3));
        socket.close();
        //jocomment
        //jocomment2
        
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final JoinRace join) throws IOException {
        this.writer = writer;
        String line = null;
        GameInit GI=null;

        send(join);
        double lastAngle=0;
        double speed=0;
        double lastInPieceDistance=0;
        int oldPiece=0;
        int MYCARINDEX=-1;
        HashSet<Integer> laneChanges=new HashSet<Integer>();
      
        while((line = reader.readLine()) != null) {
        	
        	System.out.println(line);
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
        	System.out.println(msgFromServer.msgType);
        	
            if (msgFromServer.msgType.equals("carPositions")) {
     
            	CarPositionMessage CPM =gson.fromJson(line , CarPositionMessage.class);
            	if (MYCARINDEX==-1)for (int i = 0;i<CPM.data.size();i++){if (CPM.data.get(i).id.name.equals(CONSTANTS.BOTNAME)) MYCARINDEX=i;}
            	//update currentPiece
            	int newPiece=Integer.parseInt(CPM.data.get(MYCARINDEX).piecePosition.pieceIndex);
            	//update speed
            	double newInPieceDistance=Double.parseDouble(CPM.data.get(MYCARINDEX).piecePosition.inPieceDistance);
            	double distanceToGo=GI.data.race.track.pieces.get(newPiece).length-newInPieceDistance;
            	if (oldPiece==newPiece) {
            		speed =Math.abs(newInPieceDistance-lastInPieceDistance);
            	} 
            	
            	double newAngle=Math.abs(Double.parseDouble(CPM.data.get(MYCARINDEX).angle));
            	int myFutureLane=CPM.data.get(MYCARINDEX).piecePosition.lane.endLaneIndex;
            	
            	//check if current speed matches target speed
            	Piece nextPiece=GI.nextPiece(newPiece);
            	double speeddiff = speed-nextPiece.entrySpeed[myFutureLane];
            	double ticksToGo=distanceToGo/speed;
            	double angleDiff=Math.abs(newAngle-lastAngle);
            	//if (speeddiff/ticksToGo>CONSTANTS.MAXBREAK){
            	if (doIhaveToBreak(nextPiece.entrySpeed[myFutureLane], distanceToGo, speed)){
            		if (oldPiece!=newPiece) System.out.println("Oldpiece:"+oldPiece+" Newpiece:"+newPiece+"startLane:"+CPM.data.get(MYCARINDEX).piecePosition.lane.startLaneIndex+" Endlane: "+myFutureLane);
            		System.out.println("BREAK BEFORE CURVE!!!:Speed"+speed+" Speeddiff:"+speeddiff+" distanceToGo:"+distanceToGo+" entrancespeed:"+nextPiece.entrySpeed[myFutureLane]);
            		send(new Throttle(0.0));
            	} else if (angleDiff/CONSTANTS.MAXANGLEREDUCTION>(55-lastAngle)/angleDiff){
            		send(new Throttle(0.0));
            		System.out.println("throttle down because of Angle!!!: Speed"+speed+" Speeddiff:"+speeddiff+" tickstogo:"+ticksToGo+"entrancespeed:"+nextPiece.maxEntrySpeed);
            	} else if (myFutureLane < GI.nextPiece((newPiece+1)%GI.data.race.track.pieces.size()).preferredLane
            				&& GI.nextPiece((newPiece)%GI.data.race.track.pieces.size()).Switch
            				&& speed > 0.1
            				&& !laneChanges.contains(newPiece+CPM.data.get(MYCARINDEX).piecePosition.lap*10000)){
            			send (new ChangeLane("Right"));
            			laneChanges.add(newPiece+CPM.data.get(MYCARINDEX).piecePosition.lap*10000);
            			System.out.println (" added TO VECTOR"+ (newPiece+CPM.data.get(MYCARINDEX).piecePosition.lap*10000));
            	} else if (myFutureLane > GI.nextPiece((newPiece+1)%GI.data.race.track.pieces.size()).preferredLane
            			&& GI.nextPiece((newPiece)%GI.data.race.track.pieces.size()).Switch
            			&& speed > 0.1
            			&& !laneChanges.contains(newPiece+CPM.data.get(MYCARINDEX).piecePosition.lap*10000)){
        			send (new ChangeLane("Left"));
        			laneChanges.add(newPiece+CPM.data.get(MYCARINDEX).piecePosition.lap*10000);
        			System.out.println (" added TO VECTOR"+ (newPiece+CPM.data.get(MYCARINDEX).piecePosition.lap*10000));
            	} else {
            			send(new Throttle(1.0));
            	}
            	//reset at lap
            	//if (newPiece==0)laneChanges.clear();
            	System.out.println (" MyfutureLane:"+myFutureLane+" nextLaneSwitch?:"+GI.nextPiece((newPiece)%GI.data.race.track.pieces.size()).Switch+" übernächsteLanepref:"+GI.nextPiece((newPiece+1)%GI.data.race.track.pieces.size()).preferredLane);
            	
            	//update globals
            	lastAngle=newAngle;
            	oldPiece=newPiece;
            	lastInPieceDistance=newInPieceDistance;
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init");
                 GI =gson.fromJson(line , GameInit.class);
                 GI.data.race.track.afterJson();
                
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else {
                send(new Ping());
            }
        }
    }
    public boolean doIhaveToBreak(double targetSpeed, double distance, double currentspeed){
    	//System.out.println (" doIhaveToBreak targetSpeed:"+targetSpeed+" distance:"+distance+" currentspeed:"+currentspeed);
    	double travelledDistance=0;
    	//conservative emptystep
    	double projspeed=currentspeed+CONSTANTS.MAXBREAK;
    	while (targetSpeed<projspeed){
    		travelledDistance+=projspeed;
    		projspeed-=CONSTANTS.MAXBREAK;
    		if (travelledDistance>distance) return true;
    	}
    	return false;
    }
    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}
class ChangeLane extends SendMsg {
    private String value;

    public ChangeLane(String value) {
        this.value = value;
        System.out.println("laneChange!!!: "+value);
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}
class JoinRace extends SendMsg {
	public JoinData joinData;
	public JoinRace(String name, String key, String mapName, int carCount) {
	    joinData = new JoinData (); 
	    joinData.trackName = mapName;
	    joinData.carCount = carCount;
	    joinData.botId = new BotId (); 
	    joinData.botId.key = key; 
	    joinData.botId.name = name; 
	 }
	@Override
	protected  String msgType() { 
	    return "joinRace";
	}
	@Override
	protected Object msgData() {
	    return this.joinData;
	}
}
class BotId{
	public String name;
	public String key;
}
class JoinData{
	public BotId botId;
	public String trackName;
	public int carCount;
}