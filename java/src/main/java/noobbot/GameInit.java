package noobbot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

import com.google.gson.annotations.SerializedName;

public class GameInit {
	public String msgType;
	GameInitData data;
	public Piece nextPiece(int i){
		if (data.race.track.pieces.size()==(i+1)){
			return data.race.track.pieces.get(0);
		}else{
			return data.race.track.pieces.get(i+1);
		}
	}
}
class GameInitData{
	public Race race;
}
class Race{
	public Track track;
}
class Track {
	int[] laneDistances={10 , -10};
	
	
	String id;
	String name;
	ArrayList<Piece> pieces;
	public void afterJson(){
		for (int i =0;i<pieces.size();i++){
			pieces.get(i).track=this;
			pieces.get(i).index=i;
			pieces.get(i).afterJson();
		}
		//Cascade Maxspeeds//////////////////////////////////
		for (int i =pieces.size()-2;i>=0;i--){
			//pieces.get(i).maxEntrySpeed = Math.min(pieces.get(i).maxEntrySpeed, pieces.get(i+1).maxEntrySpeed+CONSTANTS.MAXBREAK*pieces.get(i).length/pieces.get(i+1).maxEntrySpeed);
			pieces.get(i).maxEntrySpeed = Math.min(pieces.get(i).maxEntrySpeed, pieces.get(i+1).maxEntrySpeed+backtraceBreak(pieces.get(i+1).maxEntrySpeed,pieces.get(i).length));

		}
		pieces.get(pieces.size()-1).maxEntrySpeed = Math.min(pieces.get(pieces.size()-1).maxEntrySpeed, pieces.get(0).maxEntrySpeed+backtraceBreak(pieces.get(0).maxEntrySpeed,pieces.get(pieces.size()-1).length));
		//recascade for last 3rows
		for (int i =pieces.size()-2;i>=pieces.size()-5;i--){
			//pieces.get(i).maxEntrySpeed = Math.min(pieces.get(i).maxEntrySpeed, pieces.get(i+1).maxEntrySpeed+CONSTANTS.MAXBREAK*pieces.get(i).length/pieces.get(i+1).maxEntrySpeed);
			pieces.get(i).maxEntrySpeed = Math.min(pieces.get(i).maxEntrySpeed, pieces.get(i+1).maxEntrySpeed+backtraceBreak(pieces.get(i+1).maxEntrySpeed,pieces.get(i).length));

		}
		
		//Cascade PreferredLanes///////////////////////////////
		//find bestlane for first piece
		for (int i =1;i<pieces.size();i++){ if (pieces.get(i).isCurve){ pieces.get(0).preferredLane=pieces.get(i).preferredLane; break;}}

	
		pieces.get(pieces.size()-1).preferredLane = pieces.get(0).preferredLane;
		for (int i =pieces.size()-2;i>=0;i--){
			if (!pieces.get(i).isCurve)pieces.get(i).preferredLane=pieces.get(i+1).preferredLane;
		}
		for (int i =0;i<pieces.size();i++){
			System.out.println("ID:"+pieces.get(i).index+" Maxspeed:"+pieces.get(i).maxEntrySpeed+" Curve?:"+pieces.get(i).isCurve+" lenght?:"+pieces.get(i).length+" Switch?:"+pieces.get(i).Switch);
		}
		
		//put all lanes that are not set to Maxspeed of lane (should only be the cause for straights)
		for (int i =0;i<pieces.size();i++){
			Piece p =pieces.get(i);
			for (int h =0;h<p.entrySpeed.length;h++){
				if (p.entrySpeed[h]>p.maxEntrySpeed) p.entrySpeed[h]=p.maxEntrySpeed;
			}
		}
		for (int i =0;i<pieces.size();i++){
			System.out.println("ID:"+pieces.get(i).index+" perLane:"+pieces.get(i).preferredLane+" Curve?:"+pieces.get(i).isCurve+" lenght?:"+pieces.get(i).length+" Switch?:"+pieces.get(i).Switch);
		}
		Findperfectpath(pieces,0);
		for (int i =0;i<pieces.size();i++){
			System.out.println("ID:"+pieces.get(i).index+" perLane:"+pieces.get(i).preferredLane+" Curve?:"+pieces.get(i).isCurve+" lenght?:"+pieces.get(i).length+" Switch?:"+pieces.get(i).Switch);
		}
	}
	//return additional speed which can be reduced over length
	public static double backtraceBreak(double targetspeed, double length){
		double addSpeed=0; 
		double distanceEaten=0;
		while (distanceEaten<length){
			distanceEaten+=targetspeed;
			addSpeed+=CONSTANTS.MAXBREAK;
			targetspeed+=CONSTANTS.MAXBREAK;
		}
		return addSpeed;
	}
	public static void Findperfectpath(ArrayList<Piece> AL, int startingLane){
		Vector<Node> nodes=new Vector<Node>();
		//STARTNODE
		nodes.add(new Node(null,startingLane,0,0,AL.get(0).Switch));
		int nodesopened=0;
		while (nodes.get(0).piece!=AL.size()-1){
			Node best=nodes.get(0);
			Piece nextpiece=AL.get(best.piece+1);
			if (!nextpiece.Switch){
				Node newnode=new Node(best,best.lane,best.piece+1,best.cost+nextpiece.reallength[best.lane],false);
				nodes.remove(0);
				nodes.add(newnode);
			} else {
				Node newnode1=new Node(best,best.lane,best.piece+1,best.cost+nextpiece.reallength[best.lane],false);
				nodes.add(newnode1);
				if (best.lane!=0){
					Node newnode2=new Node(best,best.lane-1,best.piece+1,best.cost+Math.sqrt(CONSTANTS.SWITCHCOST*CONSTANTS.SWITCHCOST+nextpiece.length*nextpiece.length),true);
					nodes.add(newnode2);
				} else if (best.lane!=CONSTANTS.NUMOFLANES-1){
					Node newnode2=new Node(best,best.lane+1,best.piece+1,best.cost+Math.sqrt(CONSTANTS.SWITCHCOST*CONSTANTS.SWITCHCOST+nextpiece.length*nextpiece.length),true);
					nodes.add(newnode2);
				}
				nodes.remove(best);
			}
			Collections.sort(nodes,new Node());
			nodesopened++;
		}
		System.out.println("Nodes opened:"+nodesopened);
		//update bestlanes
		Node currentNode=nodes.firstElement();
		while (currentNode.father!=null){
			AL.get(currentNode.piece).preferredLane=currentNode.lane;
			currentNode=currentNode.father;
		}
	}
}
class Node implements Comparator<Node>{
	double cost;
	int lane;
	int piece;
	boolean swit;
	Node father;
	public Node(){};
	public Node(Node fath, int lane, int piece,double cost, boolean sw) {
		this.father=fath;this.lane=lane;this.piece=piece;this.cost=cost;  this.swit=sw;
	} 
	
	@Override
	public int compare(Node o1, Node o2) {
	    return (o1.cost<o2.cost ? -1 : (o1.cost==o2.cost  ? 0 : 1));
	}
}

class Piece {
	double length=0.0;
	@SerializedName("switch")
	boolean Switch=false;
	double radius=0;
	double angle=0;
	boolean isCurve=false;
	double[] entrySpeed={100000.0,20000.0};
	double[] reallength={-1,-1};
	double maxEntrySpeed=10000;
	int preferredLane=0;
	int index=-1;
	Track track;
	public void afterJson(){
		reallength[0]=length;reallength[1]=length;
		if (radius > 0){
			isCurve=true;
			int angleFactor=1;
			if (angle<0)angleFactor=-1;
			entrySpeed[0]=Math.sqrt(radius+track.laneDistances[0]*angleFactor)*CONSTANTS.RECOSPEED;
			entrySpeed[1]=Math.sqrt(radius+track.laneDistances[1]*angleFactor)*CONSTANTS.RECOSPEED;
			length=Math.abs(radius*Math.PI *angle/180);
			reallength[0]=Math.abs((radius+track.laneDistances[0]*angleFactor)*Math.PI *angle/180);
			reallength[1]=Math.abs((radius+track.laneDistances[1]*angleFactor)*Math.PI *angle/180);
			if(angle>0)preferredLane= track.laneDistances.length-1;
			System.out.println("Setting preferredLane of ID "+index+" to "+preferredLane);
			
			maxEntrySpeed=entrySpeed[preferredLane];
			//all other lanes should have
		}
		System.out.println("ID :"+index+" reallength[0]:"+reallength[0]+" reallength[1]:"+reallength[1]+" lenght:"+length);
	}

}



