package noobbot;

public class CONSTANTS {
	public static double MAXBREAK=0.13;
	public static double RECOSPEED=0.650;
	public static double MAXANGLEREDUCTION=.6;
	public static double INCREASEFACTOR=.1;
	public static String BOTNAME="";
	public static int NUMOFLANES=2;
	public static int SWITCHCOST=20;
}
